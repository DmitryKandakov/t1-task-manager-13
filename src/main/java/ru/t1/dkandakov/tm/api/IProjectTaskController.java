package ru.t1.dkandakov.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
